# Create Student Pairs to use in Canvas

The script will take a list of students (one per line) in `students.txt` and output an HTML table where each student in the list gets two other students to review the work for.

#### Run the script

1. Create a list with all the students and save it in `students.txt`. You can copy the list directly from the Grades section of Canvas.
2. Run `python create-pairs.py`
3. Open the output `pairs.html` file and copy the HTML code for the table containing the peer review assignments.
4. Go to the page in Canvas where you need to add the table and Edit it.
5. Go to HTML Editor / Raw HTML Mode and replace the existing sample table from there with the one from the `pairs.html` (including the table header)

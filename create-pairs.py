import random

table_output_file = open('pairs.html', 'w')
students = []

with open('students.txt') as students_file:
    students = students_file.readlines()

studentsCount = dict()

for i in range(0, len(students)):
    currstudent = str(students[i])
    studentsCount[currstudent] = 0

table_output_file.write("<table><tbody><tr><th>Reviewer (You) </th><th>Review work from #1 </th><th>Review work from #2 </th></tr>")

for step in range(0, len(students)):
    table_output_file.write("<tr>\n<td>" + students[step] + "&nbsp;</td>\n")
    s1 = step
    while (s1 == step or studentsCount[students[s1]] == 2):
        s1 = random.randint(0, len(students)-1)
    table_output_file.write("<td>" + students[s1] + "&nbsp;</td>\n")
    studentsCount[students[s1]] += 1
    s2 = step
    while (s2 == step or studentsCount[students[s2]] == 2 or students[s1] == students[s2]):
        s2 = random.randint(0, len(students)-1)
    table_output_file.write("<td>" + students[s2] + "&nbsp;</td>\n")
    table_output_file.write("</tr>\n")
    studentsCount[students[s2]] += 1


table_output_file.write("</table></tbody>")
table_output_file.close()
